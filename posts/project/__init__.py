from flask import Flask 
from flask_sqlalchemy import SQLAlchemy
import os   
from flask_migrate import Migrate
from project.config import BaseConfig
from flask_marshmallow import Marshmallow
db = SQLAlchemy()
migrate = Migrate()
ma = Marshmallow()


def register_blueprints(app):
    from project.posts.endpoints import post_blueprint

    app.register_blueprint(post_blueprint)

def create_app():
    app  = Flask(__name__)
    
    app.config.from_object(BaseConfig)

    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)


    register_blueprints(app)

    return app
 