
from project import ma
from project.posts.models import Post


class PostSchema(ma.SQLAlchemyAutoSchema):
    

    class Meta:
        model = Post
        load_instance = True
        load_only = ('password',)

    


post_schema = PostSchema()