from flask import Blueprint, request, jsonify
from project import db
from project.posts.models import Post
from project.posts.serializers import post_schema

post_blueprint = Blueprint('posts', __name__)


def save_post(post):
    db.session.add(post)
    db.session.commit()

@post_blueprint.route('/posts', methods = ['POST'])
def create():
    post = post_schema.load(request.get_json())

    save_post(post)

    return post_schema.dump(post), 201

@post_blueprint.route('/posts/<id>', methods=['PUT'])
def update(id):
    post = Post.query.filter_by(id=id).first()

    if post is None:
        return 'Not found', 404

    post = post_schema.load(request.get_json(), instance=post)

    save_post(post)

    return post_schema.dump(post), 200

@post_blueprint.route('/posts/<id>', methods=['PATCH'])
def patch(id):
    post = Post.query.filter_by(id=id).first()
    post = post_schema.load(request.get_json(), instance=post, partial=True)

    save_post(post)

    return post_schema.dump(post), 200

@post_blueprint.route('/posts', methods=['GET'])
def ListPost():
    posts = Post.query.all()

    return jsonify(posts_schema.dump(posts, many=True)), 200