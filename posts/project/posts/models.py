from project import db

class Post(db.Model):
    id  = db.Column(db.Integer, primary_key=True, nullable =False, autoincrement=True)
    titulo = db.Column(db.String, nullable = False)
    cuerpo = db.Column(db.String, nullable = False)
    fecha= db.Column(db.String, nullable = False)