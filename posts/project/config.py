

class BaseConfig:
    SQLALCHEMY_DATABASE_URI = 'postgres://postgres:postgres@post-db1:5432/dbposts'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class DevelopmentConfig(BaseConfig):
    SQLALCHEMY_ECHO = True