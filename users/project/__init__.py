from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from project.config import BaseConfig
from flask_marshmallow import Marshmallow
import marshmallow
from flask_cors import CORS
db = SQLAlchemy()
migrate = Migrate()
ma = Marshmallow()


def register_blueprints(app):
    from project.users.endpoints import user_blueprint

    app.register_blueprint(user_blueprint)


def register_error_handlers(app):
    @app.errorhandler(marshmallow.exceptions.ValidationError)
    def validation_error_handler(ex):
        return ex.messages, 400


def create_app():
    app = Flask(__name__)
    app.config.from_object(BaseConfig)

    CORS(app)

    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)

    register_blueprints(app)
    register_error_handlers(app)
    return app
