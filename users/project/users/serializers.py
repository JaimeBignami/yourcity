from project import ma
from project.users.models import User


class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User
        load_instance = True
        load_only = ('password',)


user_schema = UserSchema()
